import React, { useEffect, useState } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Head from 'next/head'
import { useRouter } from 'next/router'
import axios from "axios";
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ChatBubbleIcon from '@mui/icons-material/ChatBubble';
import moment from 'moment';

function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://www.exaltaretech.com/">
        Exaltare Technologies
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();
export default function Home() {
  const router = useRouter()
  const [topic, setTopic] = useState('');
  const [topicField, setTopicField] = useState('');
  const [tweets, setTweets] = useState([]);

  const getTweets = () => {
    router.push(`/?topic=${topicField}`);
    setTopic(topicField);
  };

  const topicChangeHandler = (e) => {
    setTopicField(e.target.value);
  }
  useEffect(() => {
    (async function () {
      try {
        const tweetHelper = async () => {
          let response = await axios(`https://p93w806lg4.execute-api.ap-south-1.amazonaws.com/dev/tweet?topic=${topic}&count=10`,
            {
              method: "get"
            }
          );
          setTweets(response.data.data);
        };
        await tweetHelper();
      } catch (error) {
        console.error(error.response);
      }
    })();
  }, [topic]);

  useEffect(() => {
    let searchLocation = window?.location.search;
    if (searchLocation && searchLocation.length > 0) {
      let locations = searchLocation.split("=")
      setTopic(locations[1]);
      setTopicField(locations[1]);
    }
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="sm">
        <CssBaseline />
        <Head>
          <title>Twitter champions 🦸🏻‍♂️</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component="h1" variant="h4">
            Twitter champions 🦸🏻‍♂️
          </Typography>
          <Typography component="h5" variant="h5">
            Find the best tweets from any twitter handles
          </Typography>
          <Grid container maxWidth="xs">
            <Grid item xs>
              <TextField
                margin="normal"
                size="small"
                placeholder="Input twitter handler"
                required
                fullWidth
                name="topicField"
                autoComplete='off'
                value={topicField}
                onChange={topicChangeHandler}
                autoFocus
              />
            </Grid>
            <Grid item>
              <Button
                fullWidth
                variant="contained"
                sx={{ mt: 2, ml: 1 }}
                onClick={(e) => getTweets(e)}
              >
                Search
              </Button>
            </Grid>
          </Grid>
        </Box>
        {tweets.length > 0 && tweets.map((tweet, index) => (
          <Card sx={{ maxWidth: 550, marginTop: 2 }} key={index}>
            <CardHeader
              avatar={
                <Avatar alt="Remy Sharp" src={`${tweet.user.profile_image_url_https}`} />
              }
              title={`${tweet.user.name} @${tweet.user.screen_name}`}
              subheader={moment(tweet.created_at).format("MMM Do YY")}
            />
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {`${tweet.text}`}
              </Typography>
            </CardContent>
            {tweet.extended_entities?.media[0]?.media_url_https && <CardMedia
              component="img"
              height="194"
              image={tweet.extended_entities?.media[0]?.media_url_https}
              alt="Paella dish"
            />}
            <CardActions>
              <FavoriteIcon color="disabled" fontSize="small" /> {tweet.favorite_count}
              <ChatBubbleIcon color="disabled" fontSize="small" /> {tweet.retweet_count}
            </CardActions>
          </Card>
        ))}
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}